Rails.application.routes.draw do
 resources :tweets
 root 'tweets#index'
 post '/tweets/update', to:'tweets#update'
 get '/tweets/:id/show',to: 'tweets#show'
end
