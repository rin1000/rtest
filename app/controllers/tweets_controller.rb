class TweetsController < ApplicationController
  def index
    @tweets = Tweet.all.reverse_order
  end
  
  def create
    @tweets=Tweet.create(message: params[:message],tdate: Time.current)
    redirect_to "/"
  end
  
  def destroy
    tweet = Tweet.find(params[:id])
    tweet.destroy
    redirect_to '/'
  end
  
  def show
    @tweet = Tweet.find(params[:id])
  end
  
  def edit
    @tweet = Tweet.find(params[:id])
  end
  
  def update
    tweet = Tweet.find(params[:id])
    tweet.update(message: params[:message],tdate: Time.current)
    redirect_to '/'
  end
end
